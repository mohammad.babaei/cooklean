const Hapi = require('@hapi/hapi')

const {
    mongoose
} = require('./db')

const {
    MONGO_USERNAME,
    MONGO_PASSWORD,
    MONGO_HOSTNAME,
    MONGO_PORT,
    REDIS_PASSWORD,
    REDIS_PORT,
    REDIS_HOSTNAME
  } = require('./const')

const {
    create_sample_date,
    jwtSecretCode,
    jwtValidate,
    CatboxRedis,
    defaultCache,
    hapiAuthJwt2,
    Inert,
    Vision,
    HapiSwagger,
    Crumb,
    createPlugin,
    getSummary,
    getContentType
} = require('./utils')

const userPlugin = require('./user/plugin')

const recipePlugin = require('./recipe/plugin')

//database connection
const mongoUrl = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}`
// const mongoUrl = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`
mongoose.connect(mongoUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Connected to database.")
}).catch((error) => {
    console.log("Database connection failed.", error)
})




const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: '0.0.0.0',
        cache: [{
            name: defaultCache,
            provider: {
                constructor: CatboxRedis,
                options: {
                    partition: 'cooklean_cached_data',
                    host: REDIS_HOSTNAME,
                    port: REDIS_PORT,
                    password: REDIS_PASSWORD,
                    database: 0,
                }
            }
        }]
    });

    await server.register(hapiAuthJwt2)

    // set auth strategy and default strategy for server
    server.auth.strategy('jwt', 'jwt', {
        key: jwtSecretCode,
        validate: jwtValidate
    })

    server.auth.default('jwt')

    // setting Crumb 
    // server.register({
    //     plugin: Crumb,
    //     options: {
    //         restful: true,
    //     }
    // })


    //promatheus exporter

    server.register(createPlugin({}))

    server.route({
        method: 'GET',
        path: '/metrics',
        config: {
            auth: false,
            tags: ['api'],
        },
        handler: (request, h) => {

            return h.response(getSummary()).header('Content-Type', getContentType());
        }
    })

    // swagger
    const swaggerOptions = {
        info: {
            title: 'Cooklean API Documentation',
            version: '1.0.0',
        },
        documentationPath: '/docs',
        securityDefinitions: {
            jwt: {
                type: 'apiKey',
                name: 'Authorization',
                in: 'header'
            }
        },
        security: [{
            jwt: []
        }]


    }

    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ])

    //register created plugins
    await server.register(recipePlugin, {
        routes: {
            prefix: '/recipes'
        }
    })
    await server.register(userPlugin, {
        routes: {
            prefix: '/users'
        }
    })

    // start server
    await server.start();
    console.log('Server running on %s', server.info.uri)

    // create sample data
    await create_sample_date(5, 3).then(() => {
        console.log("sample data added successfully")
    })


};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();