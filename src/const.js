const constants = {
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_HOSTNAME,
  MONGO_PORT,
  REDIS_PASSWORD,
  REDIS_PORT,
  REDIS_HOSTNAME
} = process.env

module.exports = constants