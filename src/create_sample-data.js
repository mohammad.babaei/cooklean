const {mongoose} = require('./db')
const User = require('./user/models')
const Recipe = require('./recipe/models')


function makeUsername(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

const create_sample_users = async (num) => {

    var users = []

    for (let index = 0; index < num; index++) {
        
        var userObj = {
            _id: mongoose.Types.ObjectId(),
            username: makeUsername(5),
            password: makeUsername(7)
        }
        
        users.push(userObj)

        var user_to_add = new User(userObj)

        await user_to_add.save().then(() => {
            console.log('user', index + 1, 'added')
        })
        
    }

    return users
}


const create_sample_recipes = async (userCount, recipePerUserCount) => {
    var users = await create_sample_users(userCount)
    
    users.forEach(async user => {

        userid = user._id

        for (let index = 0; index < recipePerUserCount; index++) {
        
            var recipeObj = {
                name: makeUsername(5) + 'Recipe',
                cooking_time: makeUsername(7) + 'minutes',
                prep_time: makeUsername(7) + 'minutes',
                serves: 5,
                cuisine: makeUsername(7) + 'cuisine',
                ingredients: makeUsername(7) + 'ingredients',
                directions: makeUsername(7) + 'directions',
                stars: [],
                user: userid
            }
    
            var recipe_to_add = new Recipe(recipeObj)
    
            await recipe_to_add.save()
            
        }


    })
}

module.exports = create_sample_recipes