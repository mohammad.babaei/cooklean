const {mongoose, Schema} = require('../db')

recipeSchema = new Schema({
    name: String,
    cooking_time: String,
    prep_time: String,
    serves: Number,
    cuisine: String,
    ingredients: String,
    directions: String,
    stars: [ { type: Schema.Types.ObjectId, ref: 'User' } ],
    user: { type: Schema.Types.ObjectId, ref: 'User' }
})

const Recipe = mongoose.model('Recipe', recipeSchema)

module.exports = Recipe