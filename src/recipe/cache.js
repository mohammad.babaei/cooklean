const {
    defaultCache
} = require('../utils')
const Recipe = require('./models')


const allRecipesGenerateFunc = async (cuisine) => {

    if (!cuisine) {
        var allRecipes = await Recipe.find({})
        console.log('cached all Recipes')
        return allRecipes
    }
    var recipes = await Recipe.find({
        cuisine: {
            "$regex": cuisine,
            "$options": "i"
        }
    })
    console.log('cached Recipes with cuisine: ', cuisine)
    return recipes

}

const allRecipesAndCuisineKeyGenerator = (cuisine) => {
    if (!cuisine) {
        return
    }
}
const cacheAllRecipes = {
    cache: defaultCache,
    expiresIn: 20 * 1000,
    generateTimeout: 2000
}

module.exports = {
    cacheAllRecipes,
    allRecipesGenerateFunc
}