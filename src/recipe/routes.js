const {
    allRecipesOrByCuisineHandler,
    recipeByIdHandler,
    recipeStarHandler,
    createRecipeHandler
} = require('./handlers')

const {
    recipeSchema
} = require('./schemas')

const routes = [{
        method: 'GET',
        path: '/all',
        handler: allRecipesOrByCuisineHandler,
        config: {
            tags: ['api'],
            auth: {
                strategy: 'jwt',
                scope: ['user']

            }
        }
    },
    {
        method: 'GET',
        path: '/{id}',
        handler: recipeByIdHandler,
        config: {
            tags: ['api'],
        }
    },
    {
        method: 'GET',
        path: '/{id}/star',
        handler: recipeStarHandler,
        config: {
            tags: ['api'],
        }
    },
    {
        method: 'POST',
        path: '/',
        handler: createRecipeHandler,
        config: {
            validate: {
                payload: recipeSchema
            },
            tags: ['api'],
        }
    }
]

module.exports = routes