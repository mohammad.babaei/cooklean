const { Joi } = require('../utils')

const recipeSchema = Joi.object({

    name: Joi.string().min(2).max(100).required(),
    cooking_time: Joi.string().min(2).max(100).required(),
    prep_time: Joi.string().min(2).max(100).required(),
    serves: Joi.number().min(1).required(),
    cuisine: Joi.string().min(2).max(100).required(),
    ingredients: Joi.string().min(2).max(100).required(),
    directions: Joi.string().min(2).max(100).required()

})

module.exports = {
    recipeSchema
}