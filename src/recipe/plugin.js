const recipeRoutes = require('./routes')
const {cacheAllRecipes, allRecipesGenerateFunc} = require('./cache')

const recipePlugin = {
    name: 'recipePlugin',
    version: '1.0.0',
    register: async function (server, options) {

        server.route(recipeRoutes)
        server.method('cacheAllRecipes', allRecipesGenerateFunc, {
            cache: cacheAllRecipes
        })
    }
}

module.exports = recipePlugin