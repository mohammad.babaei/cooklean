const Recipe = require('./models')
const {
    mongoose
} = require('../db')
const {
    Boom
} = require('../utils')

const {cacheAllRecipes} = require('./cache')

async function allRecipesOrByCuisineHandler(request, h) {
    cuisine = request.query.cuisine
    if (cuisine) {
        var recipes = request.server.methods.cacheAllRecipes(cuisine)
    } else {
        var recipes = request.server.methods.cacheAllRecipes('')
    }

    return recipes
}

async function recipeByIdHandler(request, h) {
    var recipeId = request.params.id
    if (mongoose.Types.ObjectId.isValid(recipeId)) {

        var recipe = await Recipe.findOne({
            _id: mongoose.Types.ObjectId(recipeId)
        })

        return recipe
    }

    return Boom.badRequest('provide valid recipe id')
}

async function recipeStarHandler(request, h) {
    var recipeId = request.params.id
    if (mongoose.Types.ObjectId.isValid(recipeId)) {

        var recipe = await Recipe.findOne({
            _id: mongoose.Types.ObjectId(recipeId)
        })

        userId = request.auth.credentials.id

        if (recipe.stars.includes(mongoose.Types.ObjectId(userId))) {
            return Boom.notAcceptable("Can't Star a recipe twice")
        }

        recipe.stars.push(userId)
        await recipe.save()

        return {
            message: 'You stared the recipe successfuly'
        }
    }
}


async function createRecipeHandler(request, h) {
    userId = request.auth.credentials.id
    recipeObj = request.payload
    recipeObj.user = userId
    var recipe = await Recipe(recipeObj).save()
    return recipe
}

module.exports = {
    allRecipesOrByCuisineHandler,
    recipeByIdHandler,
    recipeStarHandler,
    createRecipeHandler
}