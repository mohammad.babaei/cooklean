const create_sample_date = require('./create_sample-data')

const jwt = require('jsonwebtoken')

const jwtSecretCode = 'thisisarealsecretcode'

const bcrypt = require('bcrypt')

const saltRounds = 10

const Boom = require('@hapi/boom')

const User = require('./user/models')

const Joi = require('@hapi/joi')

const CatboxRedis = require('@hapi/catbox-redis')

const defaultCache = "redis_cache"

const hapiAuthJwt2 = require('hapi-auth-jwt2')

const Inert = require('@hapi/inert')

const Vision = require('@hapi/vision')

const HapiSwagger = require('hapi-swagger')

const Crumb = require('@hapi/crumb')

const { createPlugin, getSummary, getContentType } = require('@promster/hapi')

const jwtValidate = async function (decoded, request, h) {
    const user = User.exists({
        _id: decoded.id
    })
    if (!user) {
        return {
            isValid: false
        }
    } else {
        return {
            isValid: true
        }
    }
}

module.exports = {
    create_sample_date,
    jwt,
    jwtSecretCode,
    bcrypt,
    saltRounds,
    Boom,
    jwtValidate,
    Joi,
    CatboxRedis,
    defaultCache,
    hapiAuthJwt2,
    Inert,
    Vision,
    HapiSwagger,
    Crumb,
    createPlugin,
    getSummary,
    getContentType

}