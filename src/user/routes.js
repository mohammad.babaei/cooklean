const {loginHandler, registerHandler} = require('./handlers')
const {credentialSchema} = require('./schemas')
const { Joi } = require('../utils')


const routes = [{
    method: 'POST',
    path: '/register',
    config: {
        auth: false,
        tags: ['api'],
        validate: {
            payload: credentialSchema
        }
    },
    handler: registerHandler
},
{
    method: 'POST',
    path: '/login',
    config: {
        auth: false,
        tags: ['api'],
        validate: {
            payload: credentialSchema
        }
    },
    handler: loginHandler
}]

module.exports = routes