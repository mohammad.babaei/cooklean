const {mongoose, Schema} = require('../db')

const userSchema = new Schema({
    username: String,
    password: String,
    recipes: [{ type: Schema.Types.ObjectId, ref: 'Recipe' }],
    jwt_tokens: [{type: String}]

})

const User = mongoose.model('User', userSchema)

module.exports = User