const {Boom, jwt, bcrypt, jwtSecretCode, saltRounds, Joi} = require('../utils')
const {mongoose} = require('../db')
const User = require('./models')

async function loginHandler (request, h){

    const {
        username,
        password
    } = request.payload

    const user = await User.findOne({
        username: username
    })

    if (!user) {
        return Boom.unauthorized('unable to login')
    }

    const match = await bcrypt.compare(password, user.password)

    if (match) {

        const jwtPayload = {
            username,
            id: user._id,
            scope: ['user']
        }

        const jwtToken = jwt.sign(jwtPayload, jwtSecretCode, {
            expiresIn: '7 days'
        })

        user.jwt_tokens.push(jwtToken)
        await user.save()

        resObj = {
            message: 'Login successful.',
            token: jwtToken
        }

        return h.response(resObj).code(200)
    }

    return Boom.unauthorized('unable to login')

}


async function registerHandler(request, h) {

    const {
        username,
        password
    } = request.payload

    const userAlreadyExsist = await User.exists({
        username: username
    })

    if (userAlreadyExsist) {
        return Boom.conflict('Username is taken')
    }

    const _id = mongoose.Types.ObjectId()

    const jwtPayload = {
        username,
        id: _id,
        scope: ['user']
    }

    const jwtToken = jwt.sign(jwtPayload, jwtSecretCode, {
        expiresIn: '7 days'
    })

    const hashedPass = await bcrypt.hash(password, saltRounds)

    const newUserObj = {
        _id,
        username,
        password: hashedPass,
        jwt_tokens: [jwtToken]
    }

    const newUser = User(newUserObj)
    await newUser.save()

    return h.response({
        message: 'registered successfully',
        token: jwtToken
    }).code(201)

}

module.exports = {
    loginHandler,
    registerHandler
}