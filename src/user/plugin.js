const userRoutes = require('./routes')

const userPlugin = {
    name: 'userPlugin',
    version: '1.0.0',
    register: async function (server, options) {

        server.route(userRoutes)
    }
}

module.exports = userPlugin