const { Joi } = require('../utils')

const credentialSchema = Joi.object({
    username: Joi.string().min(5).max(50).lowercase().required(),
    password: Joi.string().min(7).required()
})

module.exports = {
    credentialSchema
}